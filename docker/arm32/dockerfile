FROM arm32v7/debian:stable-slim
LABEL maintainer="Jonny Rimkus <jonny@rimkus.it>"

ARG COLLABORA_DOWNLOAD_URL
ARG COLLABORA_VERSION

RUN mkdir -p /usr/share/man/man1 && \
  set -eux; export DEBIAN_FRONTEND=noninteractive && \
  apt-get update -q && \
  apt-get install -yq --no-install-recommends \
  libcap2 libcap2-bin libcap-dev libfreetype6 \
  fontconfig python3-lxml python3-polib \
  libtool g++ m4 pkg-config dpkg-dev \
  libpng++-dev automake libcppunit-dev \
  libreoffice-dev libreofficekit-dev \
  libpam0g-dev libpoco-dev npm build-essential \
  wget git ca-certificates


WORKDIR "/opt"
RUN wget -qO /tmp/collabora.tgz "${COLLABORA_DOWNLOAD_URL}" && \
 tar -xzf /tmp/collabora.tgz
WORKDIR "/opt/online-cp-${COLLABORA_VERSION}"

RUN ./autogen.sh && ./configure --enable-welcome-message \
 --enable-browsersync \
 --with-lo-path=/usr/lib/libreoffice \
 --with-lokit-path=/usr/include/LibreOfficeKit && make

EXPOSE 9983
CMD make run
